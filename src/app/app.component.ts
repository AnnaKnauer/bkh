import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ArbeitPage } from '../pages/arbeit/arbeit';
import { EnergiePage } from '../pages/energie/energie';
import { FamiliePage } from '../pages/familie/familie';
import { FragebogenPage } from '../pages/fragebogen/fragebogen';
import { IchPage } from '../pages/ich/ich';
import { KoerperPage } from '../pages/koerper/koerper';
import { NotfallkofferPage } from '../pages/notfallkoffer/notfallkoffer';
import { SchlafPage } from '../pages/schlaf/schlaf';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { WertePage } from '../pages/werte/werte';
import { WochenplanPage } from '../pages/wochenplan/wochenplan';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage; // hier die Seite ändern, welche ihr beim ersten Laden angezeigt haben wollt, am besten die Seite, an der ihr gerade arbeitet

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
