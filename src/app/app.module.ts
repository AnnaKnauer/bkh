import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ArbeitPage } from '../pages/arbeit/arbeit';
import { EnergiePage } from '../pages/energie/energie';
import { FamiliePage } from '../pages/familie/familie';
import { FragebogenPage } from '../pages/fragebogen/fragebogen';
import { IchPage } from '../pages/ich/ich';
import { KoerperPage } from '../pages/koerper/koerper';
import { NotfallkofferPage } from '../pages/notfallkoffer/notfallkoffer';
import { SchlafPage } from '../pages/schlaf/schlaf';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { WertePage } from '../pages/werte/werte';
import { WochenplanPage } from '../pages/wochenplan/wochenplan';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    WelcomePage,
    LoginPage,
    SignupPage,
    ArbeitPage,
    EnergiePage,
    FamiliePage,
    FragebogenPage,
    IchPage,
    KoerperPage,
    NotfallkofferPage,
    SchlafPage,
    TutorialPage,
    WertePage,
    WochenplanPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    LoginPage,
    SignupPage,
    ArbeitPage,
    EnergiePage,
    FamiliePage,
    FragebogenPage,
    IchPage,
    KoerperPage,
    NotfallkofferPage,
    SchlafPage,
    TutorialPage,
    WertePage,
    WochenplanPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
