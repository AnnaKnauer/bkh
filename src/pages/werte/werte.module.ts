import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WertePage } from './werte';

@NgModule({
  declarations: [
    WertePage,
  ],
  imports: [
    IonicPageModule.forChild(WertePage),
  ],
})
export class WertePageModule {}
