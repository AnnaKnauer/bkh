import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FamiliePage } from './familie';

@NgModule({
  declarations: [
    FamiliePage,
  ],
  imports: [
    IonicPageModule.forChild(FamiliePage),
  ],
})
export class FamiliePageModule {}
