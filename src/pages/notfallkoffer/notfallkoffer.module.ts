import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotfallkofferPage } from './notfallkoffer';

@NgModule({
  declarations: [
    NotfallkofferPage,
  ],
  imports: [
    IonicPageModule.forChild(NotfallkofferPage),
  ],
})
export class NotfallkofferPageModule {}
