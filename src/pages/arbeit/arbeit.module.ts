import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArbeitPage } from './arbeit';

@NgModule({
  declarations: [
    ArbeitPage,
  ],
  imports: [
    IonicPageModule.forChild(ArbeitPage),
  ],
})
export class ArbeitPageModule {}
