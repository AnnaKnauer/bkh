import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WochenplanPage } from './wochenplan';

@NgModule({
  declarations: [
    WochenplanPage,
  ],
  imports: [
    IonicPageModule.forChild(WochenplanPage),
  ],
})
export class WochenplanPageModule {}
