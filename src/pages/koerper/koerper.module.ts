import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KoerperPage } from './koerper';

@NgModule({
  declarations: [
    KoerperPage,
  ],
  imports: [
    IonicPageModule.forChild(KoerperPage),
  ],
})
export class KoerperPageModule {}
