import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchlafPage } from './schlaf';

@NgModule({
  declarations: [
    SchlafPage,
  ],
  imports: [
    IonicPageModule.forChild(SchlafPage),
  ],
})
export class SchlafPageModule {}
