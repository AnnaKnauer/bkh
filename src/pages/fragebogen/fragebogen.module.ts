import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FragebogenPage } from './fragebogen';

@NgModule({
  declarations: [
    FragebogenPage,
  ],
  imports: [
    IonicPageModule.forChild(FragebogenPage),
  ],
})
export class FragebogenPageModule {}
