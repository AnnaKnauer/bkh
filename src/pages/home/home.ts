import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { LoginPage } from '../login/login';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
rootPage:any = LoginPage;

  constructor(public navCtrl: NavController) {

  }


  logoutme() {
    //this.navCtrl.push(WelcomePage);
    this.navCtrl.push(LoginPage);


  }
}
