import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IchPage } from './ich';

@NgModule({
  declarations: [
    IchPage,
  ],
  imports: [
    IonicPageModule.forChild(IchPage),
  ],
})
export class IchPageModule {}
